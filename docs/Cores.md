# Cores

Como ven dixen antes, para crear a propiedade complexa en Java é necesario crear unha nova clase polo que lle chamei Cores.

A propiedade complexa que deseñei ten dous atributos: a cor do texto de tipo cor e a cor de fondo tamén de tipo cor.


::: tip
Recordar que o compoñente ten que ser Serializable. Para que unha clase sexa
Serializable ten que implementar Serializable e tamén teñen que ser Serializables todas as propiedades que teña. Polo tanto esta clase tamén ten que ser Serializable pois vai ser o tipo de datos dunha propiedade do compoñente.
:::

``` java{3,4}
public class Cores implements Serializable{
    
    private Color corFondo;
    private Color corTexto;

    public Cores() {
    }

    public Cores(Color corFondo, Color corTexto) {
        this.corFondo = corFondo;
        this.corTexto = corTexto;
    }
    
    

    public Color getCorFondo() {
        return corFondo;
    }

    public void setCorFondo(Color corFondo) {
        this.corFondo = corFondo;
    }

    public Color getCorTexto() {
        return corTexto;
    }

    public void setCorTexto(Color corTexto) {
        this.corTexto = corTexto;
    }
    
}
```

