# CoresPropertyEditorSupport

Unha vez creado o panel hai que ver como enlazar o panel co editor de propiedades personalizado. Este enlace faise a través dunha clase que estende de PropertyEditorSupport. Para facelo creei unha nova clase no proxecto do compoñente personalizado que a chamei CoresPropertyEditorSupport. Esta clase debe estender PropertyEditorSupport e ten que redefinir unha serie de métodos. Como mínimo debe redefinir os seguintes métodos:
## supportsCustomEditor
 Este método invócao NetBeans para comprobar se hai un editor personalizado para a propiedade. Por defecto devolve false. Neste caso debe devolver true.

## getCustomEditor
 Este método debe devolver o panel do editor personalizado. Fixarse que a clase ten unha propiedade co panel personalizado e inicializado. O método devolverá esta variable.

## getJavaInitializationString
 Devolve un String que servirá para inicializar o valor da propiedade Cores cos valores seleccionados no panel. Fixarse que se engaden os nomes dos paquetes porque non se poden incluír os imports.

## getValue
 Unha vez mostrado o panel e pulsado o botón aceptar, devolve o valor da propiedade complexa.

``` java{0}
public class CoresPropertyEditorSupport extends PropertyEditorSupport{

    private CoresPanel corespanel= new CoresPanel();
    
    @Override
    public boolean supportsCustomEditor() {
        return true; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Component getCustomEditor() {
        return corespanel; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object getValue() {
        return corespanel.getSelectedValue(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String getJavaInitializationString() {
        Cores cores =corespanel.getSelectedValue(); 
        return "new jbuttonperdonalizadoej2.Cores("+"new java.awt.Color(" +cores.getCorFondo().getRGB()+"),new java.awt.Color("+ cores.getCorTexto().getRGB()+"))";
    }
  
}
``` 