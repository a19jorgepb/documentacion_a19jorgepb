# CoresPanel

A continuación creei o panel personalizado do editor con este diseño.

![An image](./imaxes/panel.png)

Para iso creei un novo JPanel no paquete onde construín o compoñente. Puxen por nome CoresPanel (incluín o nome da propiedade no nome da clase para ter o código organizado).


Este panel está composto por dous botóns. Se pulsas no primeiro botón "..." selecionarás a cor do texto e mostrarase no 
EditText, se pulsas no segundo fará o mesmo pero seleccionando a cor de fondo do botón.

![An image](./imaxes/panelcondatos.png)

![An image](./imaxes/eligeuncolor.png)

O código donde se mostra o funcionamento é o seguinte:


``` java{11,12,13,14,15,16,17,19,20,21,22,23,24,25}
public class CoresPanel extends javax.swing.JPanel {

    private Color colorFondo;
    private Color colorTexto;
   
    public CoresPanel() {
        initComponents();
    }
    
   
    private void jButtonTextoActionPerformed(java.awt.event.ActionEvent evt) {                                             
        // TODO add your handling code here:
        JColorChooser colorChooser= new JColorChooser();
        Color color=colorChooser.showDialog(jButtonTexto, "Elije un color", colorTexto);
        colorTexto=color;
        jTextFieldCorTexto.setText("["+colorTexto.getRed()+","+colorTexto.getGreen()+","+colorTexto.getBlue()+"]");
    }                                            

    private void jButtonFondoActionPerformed(java.awt.event.ActionEvent evt) {                                             
        // TODO add your handling code here:
        JColorChooser colorChooser= new JColorChooser();
        Color color=colorChooser.showDialog(jButtonTexto, "Elije un color",colorFondo);
        colorFondo=color;
        jTextFieldCorFondo.setText("["+colorFondo.getRed()+","+colorFondo.getGreen()+","+colorFondo.getBlue()+"]");  
    }     

```
Ademais, o panel debe implementar un método público que devolva o valor da propiedade composta, é dicir, un obxecto do tipo Cores.

``` java{0}
  public Cores getSelectedValue() {
        return new Cores(colorFondo, colorTexto);
    }
```     

