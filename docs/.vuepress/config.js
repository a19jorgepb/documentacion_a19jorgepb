module.exports = {
 title: "BOTÓN PERSONALIZADO",
 description: "Descricion",
 dest: "public",
 base: "/documentacion_a19jorgepb/",
 themeConfig: {
  nav: [
   { text: 'Home', link: '/' },
   { text: 'JButtonPersonalizadoEj2', link: '/JButtonPerdonalizadoEj2' }, 
   { text: 'Cores', link: '/Cores' },
   { text: 'CoresPanel', link: '/CoresPanel' },
   { text: 'CoresPropertyEditorSupport', link: '/CoresPropertyEditorSupport' }, 
   { text: 'jButtonPersonalizadoEj2BeanInfo', link: '/jButtonPerdonalizadoEj2BeanInfo' },  
   { text: 'External', link: 'https://google.com' },
   {
       text:'Opcións',
       items:[
           {text:'Gmail',link:'https://google.com'},
           {text:'Drive',link:'https://google.com'},
       ]
   }
  ],
  lastUpdated: 'Last Updated', // string | boolean
  sidebar: [
    '/',
    ['/JButtonPerdonalizadoEj2', 'JButtonPersonalizadoEj2'],
    ['/Cores', 'Cores'],
    ['/CoresPanel', 'CoresPanel'],
    ['/CoresPropertyEditorSupport', 'CoresPropertyEditorSupport'],
    ['/jButtonPerdonalizadoEj2BeanInfo', 'jButtonPersonalizadoEj2BeanInfo']
   ]
  }
} 

