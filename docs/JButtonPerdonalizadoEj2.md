# JButtonPersonalizadoEj2

O primeiro que fixen foi crear un novo proxecto en NetBeans chamado jButtonPersonalizadoEj2 con un paquete no seu interior chamado tamén jbuttonpersonalizadoej2 e unha clase jButtonPersonalizadoEj2 a cal como vai ter as propiedades dun JButton, debe extender este compoñente.

![An image](./imaxes/ruta.png) 

``` java{0}
public class JButtonPersonalizadoEj2 extends JButton  {
}
``` 
A clase que implementa o compoñente ten que ser un JavaBean. Para que unha clase Java sexa un JavaBean ten que cumprir unhas características:

- Ten que implementar Serializable

public class JPanelImaxe extends JPanel implements Serializable {}

- Ten que ter un construtor sen parámetros.

public JPanelImaxe() {
}

``` java{0}
public class JButtonPersonalizadoEj2 extends JButton implements Serializable  {

     public JButtonPersonalizadoEj2() {
     }
}
``` 
A continuación, engadinlle a propiedade complexa pero para isto non serven os editores de propiedades que proporciona NetBeans, polo que é necesario crear un editor de propiedades personalizado.
Para crear a propiedade complexa en Java é necesario crear unha nova clase polo que lle chamei Cores.

``` java{0}
public class JButtonPersonalizadoEj2 extends JButton implements Serializable  {

    private Cores cores;

     public JButtonPersonalizadoEj2() {
     }
}
``` 
Para que o compoñente teña a propiedade composta hai que engadir os getters e setters para esta propiedade.

``` java{0}
public class JButtonPersonalizadoEj2 extends JButton implements Serializable  {

    private Cores cores;

    public JButtonPersonalizadoEj2() {
    }


    public Cores getCores() {
        return cores;
    }

    public void setCores(Cores cores) {
        this.cores = cores;
        setBackground(cores.getCorFondo());
        setForeground(cores.getCorTexto());
    }

    public Cores getCoresHover() {
        return coresHover;
    }

    public void setCoresHover(Cores coresHover) {
        this.coresHover = coresHover;
    }
}
``` 