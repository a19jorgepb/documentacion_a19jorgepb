# jButtonPersonalizadoEj2BeanInfo

Finalmente, falta unir o PropertyEditorSuport creado coa propiedade en si, porque non está enlazado de
ningunha maneira. Isto fixeno a través dun BeanInfo que crea NetBeans automaticamente. Para facelo pulsei co botón dereito sobre o compoñente JPanelImaxe -> Bean Info Editor e creei o Bean Info.

![An image](./imaxes/bean.png)

Accedín á vista de deseño da nova clase creada. Busquei a propiedade cores e na “Clase do editor de propiedades” hai que indicar a clase do panel coa ruta que inclúa tamén o paquete: jpanelimaxe.ImaxeFondoPropertyEditorSupport.class.
Se non se pon o nome da clase ben non funciona e non da ningún erro.

![An image](./imaxes/designer1.png)

![An image](./imaxes/designer2.png)

En principio xa estaría o compoñente terminado.